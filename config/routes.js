const express = require("express");
const controllers = require("../app/controllers");
const cors = require("cors");

const apiRouter = express.Router();

/**
 * TODO: Implement your own API
 *       implementations
 */
const swaggerUi = require("swagger-ui-express");
const swaggerDocument = require("./swagger.json");

apiRouter.use("/api-docs", swaggerUi.serve);
apiRouter.get("/api-docs", swaggerUi.setup(swaggerDocument));
apiRouter.use(cors());

// * Cars API
apiRouter.post(
  "/api/v1/cars",
  controllers.api.v1.users.authorize,
  controllers.api.v1.users.isAdminOrSuperAdmin,
  controllers.api.v1.cars.create
);
apiRouter.get("/api/v1/cars", controllers.api.v1.cars.list);
apiRouter.put(
  "/api/v1/cars/:id",
  controllers.api.v1.users.authorize,
  controllers.api.v1.users.isAdminOrSuperAdmin,
  controllers.api.v1.cars.update
);
apiRouter.delete(
  "/api/v1/cars/:id",
  controllers.api.v1.users.authorize,
  controllers.api.v1.users.isAdminOrSuperAdmin,
  controllers.api.v1.cars.destroy
);
apiRouter.get(
  "/api/v1/cars/:id",
  controllers.api.v1.users.authorize,
  controllers.api.v1.users.isAdminOrSuperAdmin,
  controllers.api.v1.cars.show
);

// * login API
// apiRouter.post("/api/v1/users/register", controllers.api.v1.users.register);
apiRouter.post("/api/v1/users/login", controllers.api.v1.users.login);

apiRouter.post(
  "/api/v1/users/create",
  controllers.api.v1.users.authorize,
  controllers.api.v1.users.isSuperAdmin,
  controllers.api.v1.users.create
);

apiRouter.put(
  "/api/v1/users/:id",
  controllers.api.v1.users.authorize,
  controllers.api.v1.users.isSuperAdmin,
  controllers.api.v1.users.update
);

apiRouter.get(
  "/api/v1/users/whoami",
  controllers.api.v1.users.authorize,
  controllers.api.v1.users.whoAmI
);

apiRouter.get("/api/v1/users", controllers.api.v1.users.getUsers);

/**
 * TODO: Delete this, this is just a demonstration of
 *       error handler
 */
apiRouter.get("/api/v1/errors", () => {
  throw new Error(
    "The Industrial Revolution and its consequences have been a disaster for the human race."
  );
});

apiRouter.use(controllers.api.main.onLost);
apiRouter.use(controllers.api.main.onError);

module.exports = apiRouter;
