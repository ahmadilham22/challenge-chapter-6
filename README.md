## Getting Started

Untuk mulai membuat sebuah implementasi dari HTTP Server, mulainya menginspeksi file [`app/index.js`](./app/index.js), dan lihatlah salah satu contoh `controller` yang ada di [`app/controllers/mainController.js`](./app/controllers/mainController.js)

sebelum memulai semua kegiatan pada project ini pastikan anda sudah menginstal dependenciesnya.

```sh
npm install
```

kemudian melakukan pembuatan database dan melakukan migrate.

```sh
npx sequelize db:create (digunakan untuk membuat database)
npx sequelize db:migrate (digunakan untuk menjalankan database migration)
```

Lalu untuk menjalankan development server, kalian tinggal jalanin salah satu script di package.json, yang namanya `develop`.

```sh
npm run develop
```

login menggunakan akun superadmin berikut

```sh
email : angilham2@gmail.com
password : 12345
```
