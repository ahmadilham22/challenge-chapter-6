const { Cars } = require("../models");

module.exports = {
  create(createArgs) {
    return Cars.create(createArgs);
  },

  update(id, updateArgs) {
    return Cars.update(updateArgs, {
      where: {
        id,
      },
    });
  },

  delete(id) {
    return Cars.destroy({
      where: {
        id,
      },
    });
  },

  findOne(id) {
    return Cars.finByPk(id);
  },

  findAll(id) {
    return Cars.findAll(id);
  },
};
