/**
 * @file contains entry point of controllers api v1 module
 * @author Ahmad Ilham
 */

const cars = require("./carsController");
const users = require("./usersController");

module.exports = {
  cars,
  users,
};
