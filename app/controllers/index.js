/**
 * @file contains entry point of controllers module
 * @author Ahmad Ilham
 */

const api = require("./api");

module.exports = {
  api,
};
